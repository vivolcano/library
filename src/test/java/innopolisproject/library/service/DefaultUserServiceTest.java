package innopolisproject.library.service;

import innopolisproject.library.dto.UserDto;
import innopolisproject.library.exception.ValidationException;
import innopolisproject.library.forms.User;
import innopolisproject.library.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultUserServiceTest {

    private UserService userService;
    private UserRepository userRepository;
    private UserConverter userConverter;
    private User user;

    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        userConverter = new UserConverter();
        user = new User();
        user.setLogin("testLogin");
        user.setName("testName");
        user.setId(1);
        when(userRepository.save(any())).thenReturn(user);
        userService = new DefaultUserService(userRepository, userConverter);
    }

    @Test
    void saveUserReturnUserDto() throws ValidationException {
        UserDto userDto = UserDto.builder().login("testLogin").build();
        UserDto savedUserDto = userService.saveUser(userDto);
        Assertions.assertNotNull(savedUserDto);
        Assertions.assertEquals(savedUserDto.getLogin(), "testLogin");
    }

    @Test
    void saveUserWithNullLoginThrowsValidationException()  {
        UserDto userDto = UserDto.builder().build();
        Assertions.assertThrows(ValidationException.class,
                () -> userService.saveUser(userDto),
                "Login is empty");
    }
}