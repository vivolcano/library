package innopolisproject.library.service;

import innopolisproject.library.dto.UserDto;
import innopolisproject.library.exception.ValidationException;
import innopolisproject.library.forms.User;
import innopolisproject.library.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@AllArgsConstructor
public class DefaultUserService implements UserService {

    private final UserRepository usersRepository;
    private final UserConverter userConverter;

    @Override
    public UserDto saveUser(UserDto userDto) throws ValidationException {
        validateUserDto(userDto);
        User savedUser = usersRepository.save(userConverter.fromUserDtoToUser(userDto));
        return userConverter.fromUserToUserDto(savedUser);
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public UserDto findByLogin(String login) {
        User user = usersRepository.findByLogin(login);
        if(user!=null) {
            return userConverter.fromUserToUserDto(user);
        }
        return null;
    }

    @Override
    public List<UserDto> findAll() {
        return usersRepository.findAll()
                .stream()
                .map(userConverter::fromUserToUserDto)
                .collect(Collectors
                        .toList());
    }

    private void validateUserDto(UserDto userDto) throws ValidationException {
        if(isNull(userDto)) {
             throw new ValidationException("Object user is null");
        }
        if(isNull(userDto.getLogin()) || userDto.getLogin().isEmpty()) {
            throw new ValidationException("Login is empty");
        }
    }

}
