package innopolisproject.library.service;

import innopolisproject.library.dto.UserDto;
import innopolisproject.library.exception.ValidationException;

import java.util.List;

public interface UserService {

    UserDto saveUser(UserDto usersDto) throws ValidationException;

    void deleteUser(Integer userId);

    UserDto findByLogin(String login);

    List<UserDto> findAll();
}
