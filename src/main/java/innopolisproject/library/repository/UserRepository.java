package innopolisproject.library.repository;

import innopolisproject.library.dto.UserDto;
import innopolisproject.library.forms.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository <User, Integer> {
    User findByLogin(String login);
}
